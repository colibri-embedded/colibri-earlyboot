#!/bin/bash

export RELOAD_TIMEOUT=40
# Older kernels report rpi3 wrong like this
if [ "$(get_hw_version)" == "BCM2709" ]; then
	RELOAD_TIMEOUT=15
fi
# Newer kernel (4.14.x) reports rpi3 as this
if [ "$(get_hw_version)" == "BCM2835" ]; then
	RELOAD_TIMEOUT=15
fi

## @fn custom_firstboot_begin()
## Execute custom commands upon entering firstboot
custom_firstboot_begin()
{
	true
}

## @fn custom_firstboot_end()
## Execute custom commands upon leaving firstboot
custom_firstboot_end()
{
	webui_redirect "/" ${RELOAD_TIMEOUT}
	webui_mode "redirect"
	sleep 2
}

## @fn custom_normal_begin()
## Execute custom commands upon entering earlyboot
custom_normal_begin()
{
	true
}

## @fn custom_normal_end()
## Execute custom commands upon user aborted recovery 
custom_recovery_aborted()
{
	webui_redirect "/" ${RELOAD_TIMEOUT}
	webui_mode "redirect"
	sleep 2
}

## @fn custom_normal_end()
## Execute custom commands upon leaving earlyboot
custom_normal_end()
{
	true
}

## @fn custom_recovery_begin()
## Execute custom commands upon entering emergency procedure
custom_recovery_begin()
{
	true
}

## @fn custom_recovery_end()
## Execute custom commands upon leaving emergency procedure
custom_recovery_end()
{
	webui_redirect "/" ${RELOAD_TIMEOUT}
	webui_mode "redirect"
	sleep 2
}

## @fn custom_recovery_condition()
## Condition for entering recovery mode.
custom_recovery_condition()
{
	SHOULD_ENTER_RECOVERY=$(cmdline_value colibri.recovery)
	if [ "$SHOULD_ENTER_RECOVERY" == "1" ]; then
		# turn off RECOVERY
		mount $BOOTPART -o rw /mnt/boot
		sed -i /mnt/boot/cmdline.txt -e 's/colibri.recovery=1/colibri.recovery=0/'
		umount $BOOTPART
		true
	else
		false
	fi
}

## @fn custom shutdown code
## These instructions are executed when poweroff is requested
## after all the processes have been stopped
custom_shutdown_end()
{
	true
}

## @fn custom reboot code
## These instructions are executed when reboot is requested
## after all the processes have been stopped
custom_reboot_end()
{
	true
}
