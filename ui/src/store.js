import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    socket: {
      isConnected: false,
      message: "",
      reconnect: {
        error: false,
        count: 0
      }
    },
    earlyboot: {
      progress: {
        current: 0,
        total: 0
      },
      mode: null,
      state: null,
      suggested: null,
      waiting: false,
      redirect: {
        to: null,
        timeout: 0
      },
      forced: false,
      timer: null
    }
  },

  mutations: {
    setWaiting(state) {
      // ensure boolean
      state.earlyboot.waiting = true;
    },

    clearTimer(state) {
      if (state.earlyboot.timer) clearInterval(state.earlyboot.timer);
      state.earlyboot.timer = null;
    },

    setTimer(state, timer) {
      if (state.earlyboot.timer) clearInterval(state.earlyboot.timer);
      state.earlyboot.timer = timer;
    },

    SOCKET_ONOPEN(state) {
      state.socket.isConnected = true;
      this.$socket.send("sync:");
    },

    SOCKET_ONCLOSE(state) {
      state.socket.isConnected = false;
    },

    SOCKET_ONERROR(state, event) {
      // eslint-disable-next-line
      console.error(state, event);
    },

    SOCKET_ONMESSAGE(state, message) {
      state.socket.message = message.data;
      // split message by lines
      var messages = message.data.split("\n");
      var old_mode = state.earlyboot.mode;

      for (var i = 0; i < messages.length; i++) {
        var msg = messages[i];
        var items = msg.split(":");
        var tmp;

        if (items.length == 2) {
          switch (items[0]) {
            case "mode":
              state.earlyboot.mode = items[1];
              break;
            case "state":
              state.earlyboot.state = items[1];
              if (items[1] != "welcome") {
                state.earlyboot.waiting = false;
              }
              break;
            case "redirect":
              tmp = items[1].split(",");
              state.earlyboot.redirect.to = tmp[0];
              state.earlyboot.redirect.timeout = 0;
              if (tmp.length == 2) {
                state.earlyboot.redirect.timeout = parseInt(tmp[1]);
              }
              break;
            case "suggested":
              state.earlyboot.suggested = items[1];
              break;
            case "progress":
              tmp = items[1].split("/");
              state.earlyboot.progress.current = parseInt(tmp[0]);
              state.earlyboot.progress.total = parseInt(tmp[1]);
              break;
          }
        }
      }

      var timeout = 1500;
      if (old_mode) timeout = 0;

      let new_mode = state.earlyboot.mode;

      if (old_mode != new_mode) {
        setTimeout(() => {
          switch (new_mode) {
            case "install":
              this.$router.push({ name: "install" }).catch(() => {});
              break;
            case "forced-recovery":
              state.earlyboot.forced = true;
            /* eslint-disable-next-line */
            case "recovery":
              this.$router.push({ name: "recovery" }).catch(() => {});
              break;
            case "redirect":
              this.$router.push({ name: "booting" }).catch(() => {});
              break;
            default:
              this.$router.push({ name: "loading" }).catch(() => {});
              break;
          }
        }, timeout);
      }
    },

    SOCKET_RECONNECT(state, count) {
      // console.info("Reconnecting", state, count);
      state.socket.reconnect.count = count;
    },

    SOCKET_RECONNECT_ERROR(state) {
      /* eslint-disable-next-line */
      console.error("Reconnect error", state, event);
      state.socket.reconnect.error = true;
    }
  },

  getters: {
    mode(state) {
      return state.earlyboot.mode;
    },

    state(state) {
      return state.earlyboot.state;
    },

    progress(state) {
      if (state.earlyboot.progress.total) {
        return (
          (state.earlyboot.progress.current / state.earlyboot.progress.total) *
          100
        );
      }
      return 0;
    },

    waiting(state) {
      return state.earlyboot.waiting;
    },

    redirect(state) {
      return state.earlyboot.redirect.to;
    },

    redirectTimeout(state) {
      return state.earlyboot.redirect.timeout;
    },

    suggested(state) {
      return state.earlyboot.suggested;
    }
  }
});

export default store;
