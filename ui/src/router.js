import Vue from "vue";
import VueRouter from "vue-router";

import LoadingView from "./components/Loading.vue";
import InstallView from "./components/Install.vue";
import RecoveryView from "./components/Recovery.vue";
import BootingView from "./components/Booting.vue";

Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "loading",
      component: LoadingView
    },
    {
      path: "/install",
      name: "install",
      component: InstallView
    },
    {
      path: "/recovery",
      name: "recovery",
      component: RecoveryView
    },
    {
      path: "/booting",
      name: "booting",
      component: BootingView
    }
  ]
});

export default router;
