import Vue from "vue";
//import VueWebsocket from "vue-websocket";
import VueNativeSock from "vue-native-websocket";
import store from "../store";
import router from "../router";

// let origin = location.origin.split("://")
// let origin = "http://169.154.1.2".split("://")
// origin = origin[1].split(":")

Vue.use(VueNativeSock, "ws://" + "169.254.1.2", {
  protocol: "earlyboot",
  store: store,
  reconnection: true,
  reconnectionAttempts: 5,
  reconnectionDelay: 2000
});

store.$socket = Vue.prototype.$socket;
store.$router = router;
